<?php

/**
 * @file
 * Table PDF style plugin
 */


/**
 * Style plugin to render each item as a row in a table.
 *
 * @ingroup views_style_plugins
 */
class mass_messaging_plugin_style extends views_plugin_style {
	function render() {
		
		// Group the rows according to the grouping field, if specified.
		$sets = $this->render_grouping($this->view->result, $this->options['grouping']);

		// Grab the alias of the 'id' field added by references_plugin_display.
		$id_field_alias = $this->display->handler->id_field_alias;
		
		// Check if we are in the preview mode. If so then produce a preview.
		$queue_item_id = $this->display->handler->get_option('mass_messaging_queue_item_id');
		if (empty($queue_item_id)) {
			return theme('mass_messaging_view_preview', array('view' => $this->view, 'sets' => $sets));
		}

		$results = array();
		$this->view->row_index = 0;
		foreach ($sets as $title => $records) {
			foreach ($records as $row_index => $values) {
				$results[$this->view->row_index] = new stdClass();
				// We do not want to do the rendering in the field plugin,
				// it is much simpler if we do it here.
				foreach ($this->view->field as $id => $field) {
					$results[$this->view->row_index]->{$id} = $field->theme($values);
				}
				
				// TODO: Implement the field mapping here.
				
				$results[$this->view->row_index]->mass_messaging_message_id = $values->{$id_field_alias};
				$this->view->row_index++;
			}
		}
		unset($this->view->row_index);

		return $results;
	}
}