<?php

/**
 * @file
 * PDF display plugin.
 */

/**
 * This class contains all the functionality of the PDF display.
 */
class mass_messaging_plugin_display extends views_plugin_display {

	/**
	 * Define the display type
	 */
	function get_style_type() { return 'mail'; }

	/**
	 * Disable the breadcrumb
	 */
	function uses_breadcrumb() { return FALSE; }

	function use_pager() { return FALSE; }

	function has_path() { return FALSE; }

	function execute() {
		return $this->view->render($this->display->id);
	}

	function render() {
		if (!empty($this->view->result) || !empty($this->view->style_plugin->definition['even empty'])) {
			return $this->view->style_plugin->render($this->view->result);
		}
		return '';
	}

	function uses_exposed() {
		return FALSE;
	}

	function query() {

		// Make sure the id field is included in the results, and save its alias
		// so that references_plugin_style can retrieve it.
		$this->id_field_alias = $this->view->query->add_field($this->view->base_table, $this->view->base_field);
		

		// Play nice with View UI 'preview' : if the view is not executed through
		// _*_reference_potential_references_views(), don't alter the query.
		$queue_item_id = $this->get_option('mass_messaging_queue_item_id');
		if (empty($queue_item_id)) {
			return;
		}
		
		$base_table = $this->view->query->ensure_table($this->view->base_table);
		
		
		$join = new views_join();
		$left_table = $this->view->base_table;
		$left_field = $this->view->base_field;
		$right_table = 'mass_messaging_sent_messages';
		$right_field = 'message_id';
		$type = 'LEFT';
		$extra = array(
			array(
				'field' => 'queue_item_id',
				'value' => $queue_item_id,
			)
		);
				
		$alias = $right_table . '_' . $left_table;
		
		$join->construct($right_table, $left_table, $left_field, $right_field, $extra, $type);
		
		$this->view->query->add_relationship($alias, $join, $right_table);	
		$this->view->query->add_where_expression(0, 'message_id IS NULL');
				
	}

	/**
	 * The option definition.
	 */
	function option_definition() {
		$options = parent::option_definition();

		$options['displays'] = array('default' => array());

		// Overrides for standard stuff:
		$options['style_plugin']['default'] = 'mail_table';
		$options['style_options']['default'] = array('mission_description' => FALSE, 'description' => '');
		$options['sitename_title']['default'] = FALSE;
		$options['defaults']['default']['style_plugin'] = FALSE;
		$options['defaults']['default']['style_options'] = FALSE;
		$options['defaults']['default']['row_plugin'] = FALSE;
		$options['defaults']['default']['row_options'] = FALSE;

		$options['mail_from_field']['default'] = '';
		$options['mail_subject_field']['default'] = '';
		$options['mail_text_body_field']['default'] = '';
		$options['mail_html_body_field']['default'] = '';
		$options['mail_recipient_fields']['default'] = array();
		$options['mail_cc_fields']['default'] = array();
		$options['mail_bcc_fields']['default'] = array();
		$options['mail_attachment_fields']['default'] = array();

		return $options;
	}

	function options_summary(&$categories, &$options) {
		parent::options_summary($categories, $options);

		// Remove unused sections
		//		unset($categories['pager']);
		//		unset($categories['title']);

		$categories['mail'] = array(
			'title' => t('Mass Mail Settings'),
			'column' => 'second',
			'build' => array(
				'#weight' => 20,
		),
		);

		$options['mail_recipient_fields'] = array(
			'category' => 'mail',
			'title' => t('Recipient Fields'),
			'value' => count($this->get_option('mail_recipient_fields')) ? implode(', ', $this->get_option('mail_recipient_fields')) : t('Not field defined'),
			'desc' => t('Define the recipients for the mail.'),
		);

		$options['mail_cc_fields'] = array(
			'category' => 'mail',
			'title' => t('CC Fields'),
			'value' => count($this->get_option('mail_cc_fields')) ? implode(', ', $this->get_option('mail_cc_fields')) : t('Not field defined'),
			'desc' => t('Define the carbon copy (CC) recipients of the mail.'),
		);

		$options['mail_bcc_fields'] = array(
			'category' => 'mail',
			'title' => t('BCC Fields'),
			'value' => count($this->get_option('mail_bcc_fields')) ? implode(', ', $this->get_option('mail_bcc_fields')) : t('Not field defined'),
			'desc' => t('Define the blind carbon copy (BCC) recipients of the mail.'),
		);

		$options['mail_from_field'] = array(
			'category' => 'mail',
			'title' => t('From Field'),
			'value' => $this->get_option('mail_from_field') != '' ? $this->get_option('mail_from_field') : t('Not field defined'),
			'desc' => t('Define the field, which is used for the from (sender).'),
		);
		
		$options['mail_subject_field'] = array(
			'category' => 'mail',
			'title' => t('Subject Field'),
			'value' => $this->get_option('mail_subject_field') != '' ? $this->get_option('mail_subject_field') : t('Not field defined'),
			'desc' => t('Define the field, which is used for the subject.'),
		);
		
		$options['mail_text_body_field'] = array(
			'category' => 'mail',
			'title' => t('Text Body Field'),
			'value' => $this->get_option('mail_text_body_field') != '' ? $this->get_option('mail_text_body_field') : t('Not field defined'),
			'desc' => t('Define the field for the text (no HTML) body of the message.'),
		);

		$options['mail_html_body_field'] = array(
			'category' => 'mail',
			'title' => t('HTML Body Field'),
			'value' => $this->get_option('mail_html_body_field') != '' ? $this->get_option('mail_html_body_field') : t('Not field defined'),
			'desc' => t('Define the field for the HTML body of the message.'),
		);

		$options['mail_attachment_fields'] = array(
			'category' => 'mail',
			'title' => t('Attachment Fields'),
			'value' => count($this->get_option('mail_attachment_fields')) ? implode(', ', $this->get_option('mail_attachment_fields')) : t('Not field defined'),
			'desc' => t('Define the fields, which should be added as files (attachments) to the message.'),
		);
		 

	}

	/**
	 * Option form
	 */
	function options_form(&$form, &$form_state) {
		parent::options_form($form, $form_state);

		$columns = $this->display->handler->get_field_labels();



		switch ($form_state['section']) {
			case 'mail_recipient_fields':
				$form['#title'] .= t('Recipient Fields');
				$form['mail_recipient_fields'] = array(
					'#type' => 'checkboxes',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_recipient_fields'),
					'#description' => t('Specify here the fields which build the recipient list. Fields which contains a comma, they will be separted automatically.'),
				);
				break;
			case 'mail_cc_fields':
				$form['#title'] .= t('CC Recipient Fields');
				$form['mail_cc_fields'] = array(
					'#type' => 'checkboxes',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_cc_fields'),
					'#description' => t('Specify here the fields which build the carbon copy (CC) recipient list. Fields which contains a comma, they will be separted automatically.'),
				);
				break;
			case 'mail_bcc_fields':
				$form['#title'] .= t('BCC Recipient Fields');
				$form['mail_bcc_fields'] = array(
					'#type' => 'checkboxes',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_bcc_fields'),
					'#description' => t('Specify here the fields which build the blind carbon copy (BCC) recipient list. Fields which contains a comma, they will be separted automatically.'),
				);
				break;
			case 'mail_attachment_fields':
				$form['#title'] .= t('Attachment Fields');
				$form['mail_attachment_fields'] = array(
					'#type' => 'checkboxes',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_attachment_fields'),
					'#description' => t('Specify here the fields which are added as files to the e-mail. File fields are added without any change. All other fields are added as .txt files.'),
				);
				break;
			case 'mail_subject_field':
				$form['#title'] .= t('Subject Field');
				$form['mail_subject_field'] = array(
					'#type' => 'radios',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_subject_field'),
					'#description' => t('Specify here the field which is used as the subject for the message.'),
				);
				break;
			case 'mail_from_field':
				$form['#title'] .= t('From Field');
				$form['mail_from_field'] = array(
					'#type' => 'radios',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_from_field'),
					'#description' => t('Specify here the field which is used as the from for the message.'),
				);
				break;
			case 'mail_text_body_field':
				$form['#title'] .= t('Text Body Field');
				$form['mail_text_body_field'] = array(
					'#type' => 'radios',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_text_body_field'),
					'#description' => t('Specify here the field which is used as the message body (text).'),
				);
				break;
			case 'mail_html_body_field':
				$form['#title'] .= t('HTML Body Field');
				$form['mail_html_body_field'] = array(
					'#type' => 'radios',
					'#options' => $columns,
					'#default_value' => $this->get_option('mail_html_body_field'),
					'#description' => t('Specify here the field which is used as the message body (HTML).'),
				);
				break;



		}
	}


	/**
	 * Handles the storage of the options.
	 *
	 */
	function options_submit(&$form, &$form_state) {
		// It is very important to call the parent function here:
		parent::options_submit($form, $form_state);
		switch ($form_state['section']) {
			case 'mail_recipient_fields':
				$this->set_option('mail_recipient_fields', $this->clean_up_checkbox_values($form_state['values']['mail_recipient_fields']));
				break;
			case 'mail_cc_fields':
				$this->set_option('mail_cc_fields', $this->clean_up_checkbox_values($form_state['values']['mail_cc_fields']));
				break;
			case 'mail_bcc_fields':
				$this->set_option('mail_bcc_fields', $this->clean_up_checkbox_values($form_state['values']['mail_bcc_fields']));
				break;
			case 'mail_attachment_fields':
				$this->set_option('mail_attachment_fields', $this->clean_up_checkbox_values($form_state['values']['mail_attachment_fields']));
				break;
			case 'mail_subject_field':
				$this->set_option('mail_subject_field', $form_state['values']['mail_subject_field']);
				break;
			case 'mail_from_field':
				$this->set_option('mail_from_field', $form_state['values']['mail_from_field']);
				break;
			case 'mail_text_body_field':
				$this->set_option('mail_text_body_field', $form_state['values']['mail_text_body_field']);
				break;
			case 'mail_html_body_field':
				$this->set_option('mail_html_body_field', $form_state['values']['mail_html_body_field']);
				break;
		}
	}

	/**
	 * This method is used to strip all the empty fields from the 
	 * checkboxs values.
	 * 
	 * @param array $values
	 * @return array cleaned $values
	 */
	private function clean_up_checkbox_values($values) {
		$returns = array();
		foreach ($values as $key => $value) {
			if ($value != '0') {
				$returns[$key] = $value;
			}
		}
		return $returns;
	}


}