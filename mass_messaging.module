<?php

/**
 * @file
 * This module allows the mass sending of messages over different channels (e-mail, SMS and so on)
 * by using Rules. The module provides an action which sends the messages. The selection
 * of the recipient is done with Views. The subject, the body and some other aspects of the
 * message can be generated also by Views.
 *
 */



/**
 * Implementation of hook_views_api().
 */
function mass_messaging_views_api() {
	return array(
		'api' => '3.0-alpha1',
	);
}


/**
 * Provides a list of views with a mass messaging display inside.
 * This function is used to provide the list for the rules
 * argument selection.
 */
function mass_messaging_view_list() {
	$views = views_get_all_views();

	$list = array();

	foreach($views as $view => $view_object) {
		foreach($view_object->display as $display => $display_object) {
			if ($display_object->display_plugin == 'message') {
				$list[$view . ':' . $display] = $view_object->human_name . ': ' . $display_object->display_title;
			}
		}
	}

	return $list;
}

/**
 * Implementation of hook_theme()
 */
function mass_messaging_theme() {
	return array(
		'mass_messaging_view_preview' => array(
			'variables' => array('view' => NULL, 'sets' => array()),
		)
	);
}

/**
 * Implementation of hook_menu()
 */
function mass_messaging_menu() {
	
	// Administration pages.
	$items['admin/config/mass_messaging'] = array(
		'title' => 'Mass messaging',
		'description' => 'Administer Mass messaging.',
		'position' => 'left',
		'weight' => -10,
		'page callback' => 'system_admin_menu_block_page',
		'access arguments' => array('access administration pages'),
		'file' => 'system.admin.inc',
		'file path' => drupal_get_path('module', 'system'),
	);

	$items['admin/config/mass_messaging/configuration'] = array(
		'title' => 'Configure',
		'description' => 'Configure Mass messaging module.',
		'page callback' => 'drupal_get_form',
		'access arguments' => array('administer mass messaging'),
		'page arguments' => array('mass_messaging_settings_form'),
		'file' => 'mass_messaging.admin.inc',
		'weight' => 10,
	);

	$items['admin/config/mass_messaging/queue'] = array(
		'title' => 'Queue',
		'description' => 'View the Mass messaging queue.',
		'page callback' => 'drupal_get_form',
		'access arguments' => array('view mass messaging queue'),
		'page arguments' => array('mass_messaging_queue_form'),
		'file' => 'mass_messaging.admin.inc',
		'weight' => 10,
	);
	
	return $items;
}


function mass_messaging_permission() {
	return array(
		'administer mass messaging' => array(
			'title' => t('administer mass messaging'),
			'description' => t('Administer mass messaging'),
		),
		'view mass messaging queue' => array(
			'title' => t('view mass messaging queue'),
			'description' => t('View the mass messaging queue.'),
		),
	);
}



/**
 * This function sends messages to the recipients. It messages are only
 * queued. The real sending is done later by the cron.
 *
 * @param string $view_id
 * @param string $display_id
 * @param array $arguments
 */
function mass_messaging_send_messages($view_id, $display_id, $arguments = array()) {
	db_insert('mass_messaging_queue')
	->fields(array(
		'view_id' => $view_id,
		'display_id' => $display_id,
		'arguments' => serialize($arguments),
	))
	->execute();
}

/**
 * Implementation of hook_cron() 
 * 
 * This method is used to process the messages in the queue.
 */
function mass_messaging_cron() {
	
	// The cron percentage defines how long, of the available time the
	// cron can take for the processing of the mails in the queue. Default
	// 80, so other application get also a chance to process anything.
	$cron_percentage = variable_get('mass_messaging_cron_percentage', '80');
	
	// This timeout indicates how long a queue cannot show any
	// activity until it is seen as a broken. After each 
	// batch the queue is updated.
	$timeout = variable_get('mass_messaging_queue_timeout', '180');
	
	// The batch size determine how many messages are processed 
	// with in a iteration.
	$batch_size = variable_get('mass_messaging_batch_size', '100');
	
	$total_seconds = ini_get('max_execution_time');
	if (empty($total_seconds)) {
		// If we can't get any max execution time, then we set it to 30 seconds. 
		// because this is the default max execution time.
		$total_seconds = 30;
	}
	
	$lost_seconds = timer_read('page')/1000;
	$available_seconds = $total_seconds - $lost_seconds;
	
	// Get the real usable time for processing. We give other application at least 5 seconds
	// to process anything. This ensures that any user input wont hurt the application.
	$usable_seconds = min(array($available_seconds - 5, $total_seconds*$cron_percentage/100));
		
	// Process as long as we have enough time. 
	while (timer_read('page')/1000 < $lost_seconds + $usable_seconds) {
	
		$transaction = db_transaction();
		try {
				
			$result = db_query("SELECT 
				queue_item_id, 
				view_id, 
				display_id, 
				arguments 
			FROM 
				{mass_messaging_queue} 
			WHERE 
				(is_processing = 0 OR last_process_activity + :timeout < NOW())
			ORDER BY last_process_activity ASC
			LIMIT 0,1", array(':timeout' => $timeout));
			
			$queue_item = $result->fetchObject();
			if (!empty($queue_item)) {
				db_update('mass_messaging_queue')
					->fields(array(
						'is_processing' => 1,
						'last_process_activity' => time(),
					))
					->condition('queue_item_id', $queue_item->queue_item_id, '=')
					->execute();
			}
			
			// Force the commit
			$transaction->__destruct();
				
		} catch (Exception $e) {
			$transaction->rollback();
			watchdog('mass_messaging', $e->getMessage(), array(), WATCHDOG_ERROR);
			
			// We are in a error state, so stop and try to take another queue item.
			break;
		}
		unset($transaction);
		
		
		// If no queue item is set, then the queue seems to be empty. So nothing
		// to process.
		if (empty($queue_item)) {
			return;
		}	
		
		// Start processing the queue item
		// Processing steps:
		// 1. Load queue item view with display & arguments (Left outer join is in the display plugin).
		// 2. Iterates over the message in the view
		// 3. Start Transaction
		// 4. Insert message to the messages_sent table
		// 5. Send the message
		// 6. Commit
		// 7. take next message until there is no message in the view.
		
		$view = views_get_view($queue_item->view_id);
	
		// Try to load the display
		if (!$view->set_display($queue_item->display_id)) {
			watchdog('mass_messaging', "Can't load display '" . $queue_item->display_id . "'.");
			return;
		}
	
		$view->display_handler->set_option('mass_messaging_queue_item_id', $queue_item->queue_item_id);
		
		$no_messages_left_in_view = false;
			
		// Limit result set size by the batch_size
		$view->display_handler->set_option('pager', array('type' => 'some', 'options' => array('items_per_page' => $batch_size)));
	
		// Make sure the query is not cached
		$view->is_cacheable = FALSE;
	
		// Get the messages
		$messages = $view->execute_display($queue_item->display_id, unserialize($queue_item->arguments));
			
		$number_of_messages = count($messages);
		
		// Check if there is time left for processing. This is required, because
		// complex queries can lead to long loading times. Especially when the batch
		// size is too big.
		if (!(timer_read('page')/1000 < $lost_seconds + $usable_seconds)) {
			
		}	
		elseif (empty($messages) || $number_of_messages <= 0) {
			$no_messages_left_in_view = true;
		}
		else {
		
			// Debuggin:
			// use watchdog('mass_messaging', (string)$view->query->query()); to log the query.
	
			$i = 0;
			while($number_of_messages > $i && (timer_read('page')/1000 < $lost_seconds + $usable_seconds)) {
				$message_item = $messages[$i];
					
				$transaction = db_transaction();
				try {
					
					db_insert('mass_messaging_sent_messages')
					->fields(array(
						'queue_item_id' => $queue_item->queue_item_id,
						'message_id' => $message_item->mass_messaging_message_id,
						'message_sent_on' => time(),
					))
					->execute();
					
					
					if (variable_get('mass_messaging_send_method', 'mimemail') == 'debug') {
						$log = '';
						
						// TODO: Log the message in the watchdog
						$log .= '<h3>' . t('Debug Information') . '</h3>';
						$log .= '<strong>Queue Item ID:</strong> ' . $queue_item->queue_item_id . '<br /> ';
						$log .= '<strong>View:</strong> ' . $queue_item->view_id . '<br /> ';
						$log .= '<strong>Display:</strong> ' . $queue_item->display_id . '<br /> ';
						
						
						watchdog('mass_messaging', $log, array(), WATCHDOG_DEBUG);
					}
					
					else {
						// TODO: Get the correct field mappings and implement the send method:
//						
//						$to = str_replace(array("\r", "\n"), '', $to);
//						$cc = str_replace(array("\r", "\n"), '', $cc);
//						$bcc = str_replace(array("\r", "\n"), '', $bcc);
//						$from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;
//				
//						/*$attachments_string = trim($attachments);
//						if (!empty($attachments_string)) {
//							$attachments = array();
//							$attachment_lines = array_filter(preg_split("/\n/", trim($attachments_string)));
//							foreach ($attachment_lines as $key => $attachment_line) {
//								$attachment = explode(":", trim($attachment_line), 2);
//								$attachments[] = array(
//									'filepath' => $attachment[1],
//									'filemime' => $attachment[0],
//								);
//							}
//						}*/
//				
//						$params = array(
//							'context' => array(
//								'subject' => $subject,
//								'body' => $body,
//								'action' => $element,
//								'state' => $state,
//							),
//							'plaintext' => $plaintext,
//							'attachments' => $attachments,
//						);
//				
//						if ($cc) $params['headers']['Cc'] = $cc;
//						if ($bcc) $params['headers']['Bcc'] = $bcc;
//				
//						// Set a unique key for this mail.
//						$name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
//						$key = 'rules_action_mimemail_' . $name . '_' . $element->elementId();
//						
//						// TODO: Check if we can replace the language_default() by a better method (get it from view or something similar).
//						$message = drupal_mail('mimemail', $key, $to, language_default(), $params, $from);
						
						// TODO: Remove (only for debug)
						$message['result'] = true;
						if (!$message['result']) {
							// Send operation failed
							throw new Exception("The message could not be sent.");
						}
						
					}
					
					
				} catch (Exception $e) {
					$transaction->rollback();
					watchdog('mass_messaging', $e->getMessage(), array(), WATCHDOG_ERROR);
				}
				
				// Force Commit
				$transaction->__destruct();
				
				unset($transaction);
				$i++;
			}
		}
		
		// Since we have processed all message in this view
		// we can remove the item from the view
		if ($no_messages_left_in_view || $number_of_messages <= $i) {
			db_delete('mass_messaging_queue')
				->condition('queue_item_id', $queue_item->queue_item_id, '=')
				->execute();
			db_delete('mass_messaging_sent_messages')
				->condition('queue_item_id', $queue_item->queue_item_id, '=')
				->execute();
		}
		else {
			// Update the queue activity
			db_update('mass_messaging_queue')
				->fields(array(
					'is_processing' => '0',
					'last_process_activity' => time(),
				))
				->condition('queue_item_id', $queue_item->queue_item_id, '=')
				->execute();
		}		
	}
	

}


function theme_mass_messaging_view_preview($vars) {
	$view = $vars['view'];
	$sets = $vars['sets'];
	
	$rows = array();
	
	// TODO: Implement the header
	$header = array();
	
	// TODO: Implement the filter for relevant fields (subject, from etc.)
	$view->row_index = 0;
	foreach ($sets as $title => $records) {
		foreach ($records as $row_index => $values) {
			$cells = array();
			foreach ($view->field as $id => $field) {
				$cells[] = $field->theme($values);
			}
			$rows[] = $cells;
			$view->row_index++;
		}
	}
	unset($view->row_index);
	
	return theme_table(array(
		'rows' => $rows, 
		'header' => $header, 
		'attributes' => array(), 
		'caption' => '', 
		'colgroups' => array(), 
		'sticky' => false, 
		'empty' => t('No matching entry found.'),
	));	
}




